# R web scraper examples

This repository includes a bunch of scripts to scrape data from web pages
using R. All of them make use of `rvest` library.

## Prerequisites

You need to have R installed on your machine. Please refer to the [official
documentation](https://cran.r-project.org/doc/manuals/R-admin.html) to do so.

The following steps use the `Rscript` command,
which is part of the R suite, to run the web scraping scripts.

Moreover, you need to install the following R libraries from an R
interactive shell (accessible by launching `R` on the command line):

```
> install.packages("tidyverse")
> install.packages("rvest")
> install.packages("stringr")
> install.packages("rebus")
> install.packages("lubridate")
```

## Fire Authority committee members

Navigate to the `firerescue` directory and run the following command:

```
Rscript firerescue.R
```

It will parse commitee members data from the [https://fireauthority.dsfire.gov.uk/](https://fireauthority.dsfire.gov.uk/)
website and scrape all the meetings they have been involved since 01/01/1990.

The starting date can be modified by editing the `starting_day`, `starting_month`
and `starting_year` parameters.

The result is stored in CSV format in the `firerescue/result.csv` file. There is already a sample file in the directory.

## Trust Pilot

Navigate to the `trustpilot` directory. This includes a single R script that will parse Amazon client reviews on Trust Pilot and write a series of data in a TSV file.

**N.B:** This script is just a reference code to acquire the basics of `rvest` and other R plugins for string manipulation and data processing. It is not meant to be executed and may fail unexpectedly.